from conans import CMake, ConanFile


class ConanLibrary(ConanFile):
    name = "{{cookiecutter.library_name}}"
    version = "0.0.0"
    description = "{{cookiecutter.repository_description}}"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": True}
    generators = "cmake"
    exports_sources = "doc*", "include*", "lib*", "tests*", "CMakeLists.txt"

    def requirements(self):
        self.requires.add("spdlog/1.8.2")
        self.requires.add("doxygen/1.8.20")
        self.requires.add("gtest/1.10.0")

    def configure(self):
        self.settings.compiler.libcxx = "libstdc++11"

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder=".")
        cmake.build()

    def package(self):
        self.copy("*.h", src="include/{{cookiecutter.library_name}}", dst="include/{{cookiecutter.library_name}}")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so*", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["{{cookiecutter.library_name}}"]
