#include <gtest/gtest.h>
#include <internal_library/internal_library.h>

using namespace internal_library;

TEST(InternalLibrary, isThisAnInternalLibrary) {
  InternalLibrary internal_library;
  ASSERT_EQ(internal_library.isThisAnInternalLibrary(), true);
}
