#include <gtest/gtest.h>
#include <{{cookiecutter.library_name}}/library_part.h>

using namespace {{cookiecutter.library_name}};

TEST(LibraryPart, isThisALibraryPart) {
  LibraryPart library_part;
  ASSERT_EQ(library_part.isThisALibraryPart(), true);
}
