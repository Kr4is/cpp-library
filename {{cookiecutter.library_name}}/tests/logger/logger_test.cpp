#include <gtest/gtest.h>
#include <logger/logger.h>

using namespace logger;

TEST(Logger, KLoggerName) {
  ASSERT_EQ(KLoggerName, "{{cookiecutter.library_name}}");
}