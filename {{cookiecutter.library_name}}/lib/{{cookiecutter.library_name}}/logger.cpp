#include <logger/logger.h>
#include <{{cookiecutter.library_name}}/logger.h>

using namespace {{cookiecutter.library_name}};

std::shared_ptr<spdlog::logger> setupLogger(
    std::vector<spdlog::sink_ptr> sinks) {
  auto logger = spdlog::get(logger::KLoggerName);
  if (not logger) {
    if (sinks.size() > 0) {
      logger = std::make_shared<spdlog::logger>(
          logger::KLoggerName, std::begin(sinks), std::end(sinks));
      spdlog::register_logger(logger);
    } else {
      logger = spdlog::stdout_color_mt(logger::KLoggerName);
    }
  }
  return logger;
}
