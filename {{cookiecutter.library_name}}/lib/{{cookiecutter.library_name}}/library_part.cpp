#include <{{cookiecutter.library_name}}/library_part.h>

using namespace {{cookiecutter.library_name}};

LibraryPart::LibraryPart() {
  std::make_shared<spdlog::logger>(
      "LibraryPart", std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
  library_part_private_ = 1;
}

LibraryPart::~LibraryPart() { library_part_private_ = 2; }

bool LibraryPart::isThisALibraryPart() { return true; }
