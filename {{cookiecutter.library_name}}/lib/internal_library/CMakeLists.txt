project(internal_library)

add_library(${PROJECT_NAME} internal_library.cpp)

target_link_libraries(${PROJECT_NAME} ${CONAN_LIBS})
