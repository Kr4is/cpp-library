#include <logger/logger.h>

using namespace logger;

std::shared_ptr<spdlog::logger> getLogger() {
  auto logger = spdlog::get(KLoggerName);
  if (not logger) {
    logger = spdlog::stdout_color_mt(KLoggerName);
  }
  return logger;
}
