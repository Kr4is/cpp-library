#ifndef {{cookiecutter.library_name.upper()}}_{{cookiecutter.library_name.upper()}}_H
#define {{cookiecutter.library_name.upper()}}_{{cookiecutter.library_name.upper()}}_H

#include <{{cookiecutter.library_name}}/library_part.h>
#include <{{cookiecutter.library_name}}/logger.h>

#endif  // {{cookiecutter.library_name.upper()}}_{{cookiecutter.library_name.upper()}}_H
