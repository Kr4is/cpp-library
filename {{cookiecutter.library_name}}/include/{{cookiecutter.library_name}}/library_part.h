#ifndef {{cookiecutter.library_name.upper()}}_LIBRARY_PART_H
#define {{cookiecutter.library_name.upper()}}_LIBRARY_PART_H

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

namespace {{cookiecutter.library_name}} {

/// \brief Example library class
class LibraryPart {
 public:
  LibraryPart();
  ~LibraryPart();

  /// \brief Example public library function
  /// \return return true
  bool isThisALibraryPart();

 private:
  std::shared_ptr<spdlog::logger> _logger;
  int library_part_private_ = 0;
};

}  // namespace {{cookiecutter.library_name}}

#endif  // {{cookiecutter.library_name.upper()}}_LIBRARY_PART_H
