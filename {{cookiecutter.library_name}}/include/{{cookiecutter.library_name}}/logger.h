#ifndef {{cookiecutter.library_name.upper()}}_LOGGER_H
#define {{cookiecutter.library_name.upper()}}_LOGGER_H

#include <spdlog/spdlog.h>
#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace {{cookiecutter.library_name}}
{
    std::shared_ptr<spdlog::logger> setupLogger(std::vector<spdlog::sink_ptr> sinks);
} // namespace {{cookiecutter.library_name}}

#endif // {{cookiecutter.library_name.upper()}}_LOGGER_H
