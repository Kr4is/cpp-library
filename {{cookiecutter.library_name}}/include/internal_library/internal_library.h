#ifndef INTERNAL_LIBRARY_INTERNAL_LIBRARY_H
#define INTERNAL_LIBRARY_INTERNAL_LIBRARY_H

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

namespace internal_library {

/// \brief Example internal library class
class InternalLibrary {
 public:
  InternalLibrary();
  ~InternalLibrary();

  /// \brief Example public library function
  /// \return return true
  bool isThisAnInternalLibrary();

 private:
  std::shared_ptr<spdlog::logger> _logger;
  int internal_library_private_ = 0;
};

}  // namespace internal_library

#endif  // INTERNAL_LIBRARY_INTERNAL_LIBRARY_H
