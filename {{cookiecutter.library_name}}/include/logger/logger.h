#ifndef LOGGER_LOGGER_H
#define LOGGER_LOGGER_H

#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

namespace logger {
static const std::string KLoggerName = "{{cookiecutter.library_name}}";

std::shared_ptr<spdlog::logger> getLogger();
}  // namespace logger

#endif  // LOGGER_LOGGER_H
