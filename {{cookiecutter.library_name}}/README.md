# {{cookiecutter.repository_name}}

{{cookiecutter.repository_description}}.

## Requirements

- Conan >= 1.43.2
- Cmake >= 3.20.0

## Build

```bash
mkdir build && cd build
cmake ..
make -j$(nproc)
```

### Add conan remotes

```bash
conan remote add <remote-name> <remote-url>
```

### Create

```bash
conan create . <repository-path> --build={{cookiecutter.library_name}} -s compiler.libcxx=libstdc++11
```

### Upload

```bash
conan upload {{cookiecutter.library_name}}/0.0.0@<repository-path> -r <remote-name>
```

