# Cookiecutter Template for C++ Library
![](.logo.png)

## Requirements

- `cookiecutter`: [Documentation](https://cookiecutter.readthedocs.io/en/latest/index.html):
	- Install with pip: `pip install cookiecutter`
	- Install with conda: `conda install -c conda-forge cookiecutter`
	- Install with apt-get: `sudo apt-get install cookiecutter`

- `cruft`: [Documentation](https://cruft.github.io/cruft/):
	- Install with pip: `pip install cruft`

## Usage

## Create a new library

```bash
cruft create https://gitlab.com/Kr4is/cpp-library.git
```

## Update a library

```bash
cruft update
```

## Link an existing library from cookiecutter

```bash
cruft link https://gitlab.com/Kr4is/cpp-library.git
```
